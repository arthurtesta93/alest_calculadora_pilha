import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class App{

    private static Calculadora c;

    public App(){
        c = new Calculadora();
    }

    public static void main(String[] args){
        App app = new App();
        app.executaCalculadora("entrada_exemplo1");
        app.executaCalculadora("entrada_exemplo2");
        app.executaCalculadora("entrada_exemplo3");
        app.executaCalculadora("entrada_exemplo4");
    }

    /**
    * Executa a leitura do arquivo txt e chama o metodo adequado
    @param fileName nome do arquivo txt a ser carregado 
    */
    public void executaCalculadora(String fileName){
        c.clear();
        System.out.println("Inicializando leitura do arquivo "+fileName);
        String comandoCalculadora;
        String currDir = Paths.get("").toAbsolutePath().toString();
        // Monta o nome do arquivo
        String nameComplete = currDir+"\\"+fileName+".txt";
        // Cria acesso ao "diretorio" da mídia (disco)
        Path path = Paths.get(nameComplete);
        // Usa a classe scanner para fazer a leitura do arquivo
        try (Scanner sc = new Scanner(Files.newBufferedReader(path, StandardCharsets.UTF_8))){
            sc.useDelimiter("[;\n]"); // separadores: ; e nova linha
            while (sc.hasNext()) {
                comandoCalculadora = sc.next();
                //System.out.println(comandoCalculadora);
                comandoCalculadora = comandoCalculadora.trim();
                //System.out.println(comandoCalculadora.length());
                if(comandoCalculadora.chars().allMatch(Character::isDigit)){
                    //System.out.println(" e numero");
                    int e = Integer.parseInt(comandoCalculadora);
                    c.addInteiro(e);
                }
                else{
                    //System.out.println(" n e numero");
                    operacao(comandoCalculadora);
                }
            }
            c.singleResult();
            c.printResult();
        }
        catch (IOException x){
            System.err.format("Erro de E/S: %s%n", x);
        }
        
        catch(ArithmeticException x){
            System.err.format("Erro aritmetico: %s%n", x);
        }

        catch(IllegalArgumentException x){
            System.err.format("Erro de input: %s%n", x);
        }
    }

    public static void operacao(String comandoCalculadora){

        switch(comandoCalculadora){
            case "+":
            c.soma();
            //System.out.println("Faz soma");
            break;
            case "-":
            //System.out.println("Faz sub");
            c.subtr();
            break;
            case "*":
            //System.out.println("Faz mult");
            c.multipl();
            break;
            case "/":
            //System.out.println("Faz div");
            c.div();
            break;
            case "pop":
            //System.out.println("Faz pop");
            c.pop();
            break;
            case "dup":
            //System.out.println("Faz dup");
            c.dup();
            break;
            case "swap":
            //System.out.println("Faz swap");
            c.swap();
            break;
            case "chs":
            //System.out.println("Faz chs");
            c.chs();
            break;
            case "sqrt":
            //System.out.println("Faz sqrt");
            c.sqrt();
            break;
            default:
            System.out.println("Comando invalido");
            throw new IllegalArgumentException();
        }
    }
}