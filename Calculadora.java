public class Calculadora{
    private Pilha p;

    public Calculadora(){
        p = new Pilha();
    }

    /**
     * Adiciona numero a pilha.
     * @param e numero a ser adicionado
     */
    public void addInteiro(int e){
       p.push(e);
    }
    /**
     * Obtem dois numeros topo pilha, os soma e push o resultado 
     *  @throws ArithmeticException se nao ha dois numeros na pilha
     */
    public void soma(){
        if(p.size() < 2 ) {
    		System.out.println("Necessario mais de um elemento na pilha");
    		throw new IllegalArgumentException();
    	}
        double primeiroElemento = p.pop();
        double segundoElemento = p.pop();
        double resultado = primeiroElemento + segundoElemento;
        p.push(resultado);
    	
    }
    
    /**
     * Obtem numero topo da pilha, subtrai pelo numero topo atual e push o resultado 
     *  @throws ArithmeticException se nao ha dois numeros na pilha
     */
    public void subtr(){
        if(p.size() < 2 ) {
    		System.out.println("Necessario mais de um elemento na pilha");
    		throw new IllegalArgumentException();
    	}
        double primeiroElemento = p.pop();
        double segundoElemento = p.pop();
    	
    	double resultado = primeiroElemento - segundoElemento;
    	p.push(resultado);

    }

    /**
     * Obtem dois numeros topo pilha, multiplica um pelo outro e push o resultado 
     *  @throws ArithmeticException se nao ha dois numeros na pilha
     */
    public void multipl(){
        if(p.size() < 2 ) {
    		System.out.println("Necessario mais de um elemento na pilha");
    		throw new IllegalArgumentException();
    	}
        double primeiroElemento = p.pop();
        double segundoElemento = p.pop();
        
        double resultado = primeiroElemento * segundoElemento;
        p.push(resultado);
    	
    }

    /**
     * Obtem numero topo da pilha, divide pelo numero topo atual e push o resultado 
     *  @throws ArithmeticException se nao ha dois numeros na pilha
     */
    public void div(){
        if(p.size() < 2 ) {
    		System.out.println("Necessario mais de um elemento na pilha");
    		throw new IllegalArgumentException();
    	}
        double primeiroElemento = p.pop();
        double segundoElemento = p.pop();
        
        double resultado = primeiroElemento / segundoElemento;
        p.push(resultado);
    	
    }

    /**
     * Descarta numero topo da calculadora
     *  @throws IllegalArgumentException se nao houver nenhum numero na pilha
     */
    public void pop(){
        if(p.isEmpty()) {
    		System.out.println("Nenhum numero na calculadora");
    		throw new IllegalArgumentException();
    	}
    	p.pop();
    }

    /**
     * Push uma copia do numero topo da calculadora
     *  @throws IllegalArgumentException se nao houver nenhum numero na pilha
     */
    public void dup(){
    	
    	if(p.isEmpty()) {
    		System.out.println("Nao existem elementos para duplicar");
    		throw new IllegalArgumentException();
    	}
    	double numeroDuplicado = p.top();
    	p.push(numeroDuplicado);
    }

    public void singleResult(){
        if(p.size()>1){
            System.out.println("Erro: mais de um numero na memoria ao fim do arquivo.");
        }
        else{
            System.out.println("Apenas um numero armazenado em memoria");
        }
    }

    /**
     * Troca de posicao os dois numeros do topo da pilha
     *  @throws IllegalArgumentException se nao houver pelo menos dois numeros na pilha
     */
    public void swap(){
    	
    	if(p.size() < 2 ) {
    		System.out.println("Necessario mais de um elemento na pilha");
    		throw new IllegalArgumentException();
    	}
        double primeiroElemento = p.pop();
        double segundoElemento = p.pop();
    	p.push(primeiroElemento);
    	p.push(segundoElemento);

    }

    /**
     * Troca sinal do numero topo da calculadora
     *  @throws IllegalArgumentException se nao houver nenhum numero na pilha
     */
    public void chs(){

    	if(p.isEmpty()) {
    		System.out.println("Nenhum numero na calculadora");
    		throw new IllegalArgumentException();
    	}
        double primeiroElemento = p.pop();
    	// change signal
    	p.push(primeiroElemento * (-1));
    
    }

    /**
     * Faz raiz quadrada do  numero topo da calculadora e push o resultado
     *  @throws IllegalArgumentException se nao houver nenhum numero na pilha
     */
    public void sqrt(){
    	if(p.isEmpty()) {
    		System.out.println("Nenhum elemento armazenado na calculadora");
    		throw new IllegalArgumentException();
        }	
        if(p.top() < 0){
            System.out.println("Numero negativo, nao e possivel realizar raiz quadrada");
    		throw new ArithmeticException();
        }
        double primeiroElemento = p.pop();
        double resultado = Math.sqrt(primeiroElemento);
        p.push(resultado);
        
    }
    /**
     * Limpa pilha da calculadora
     */
    public void clear(){
    	p.clear();
    }

    public void printResult(){
 
    	System.out.println("Resultado armazenado no topo da pilha:" + p.top());
        System.out.println("Tamanho maximo da pilha: " + p.tamMaximo());
    }
}