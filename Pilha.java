public class Pilha{

    private int count;
    private int max_size;
    private Node header;
    private Node trailer;

    private class Node {
        public Double element;
        public Node next;
        public Node prev;
        public Node(Double e) {
            element = e;
            next = null;
            prev = null;
        }
    }


    /**
     * Construtor da Pilha
     */
    public Pilha(){
        header = new Node(null);
        trailer = new Node(null);
        header.next = trailer;
        trailer.prev = header;
        count = 0;
        max_size = 0;
    }

    /**
     * Push numero ao topo da pilha, atualiza count e, se necessario, max_size
     * @param e numero a ser adicionado ao topo da pilha
     */
    public void push(double e){
        Node n = new Node(e);
        n.prev = trailer.prev;
        n.next = trailer;
        trailer.prev.next = n;
        trailer.prev = n;
        count++;
        if(count > max_size){
            max_size = count;
        }
    }
    /**
     * Remove numero do topo da pilha, atualiza count
     * @return numero removido
     * @throws IllegalArgumentException se nao houver numero a ser removido
     */
    public double pop(){
        if(count==0){
            System.out.println("Nao ha numero a ser removido");
            throw new IllegalArgumentException();
        }
        Node aux = trailer.prev;
        aux.prev.next = aux.next;
        aux.next.prev = aux.prev;
        count--;
        return aux.element; //temp
    }
    /**
     * Retorna (mas nao remove) elemento topo da pilha
     * @return numero topo
     */
    public double top(){
        Node aux = trailer.prev;
        return aux.element;
    }

    /**
     * Retorna tamanho pilha
     * @return count
     */
    public int size(){
        return count;
    }
    /**
     * Retorna tamanho maximo pilha
     * @return max_size
     */
    public int tamMaximo(){
        return max_size;
    }
    /**
     * @return true se pilha vazia, false caso contrario
     */
    public boolean isEmpty(){
        if(count==0){
            return true;
        }
        return false;
    }
    /**
     * Esvazia pilha, atualiza count e max_size
     */
    public void clear(){
        header = new Node(null);
        trailer = new Node(null);
        header.next = trailer;
        trailer.prev = header;
        count = 0;
        max_size = 0;
    }

}